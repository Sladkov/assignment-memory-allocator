CFLAGS=--std=c17 -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror -DDEBUG
BUILDDIR=build
SRCDIR=src
CC=gcc

all: $(BUILDDIR)/mem.o $(BUILDDIR)/util.o $(BUILDDIR)/mem_debug.o $(BUILDDIR)/main.o $(BUILDDIR)/test1.o $(BUILDDIR)/test2.o $(BUILDDIR)/test3.o $(BUILDDIR)/test4.o
	$(CC) -o $(BUILDDIR)/main $^

build:
	mkdir -p $(BUILDDIR)

$(BUILDDIR)/mem.o: $(SRCDIR)/mem.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/mem_debug.o: $(SRCDIR)/mem_debug.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/util.o: $(SRCDIR)/util.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/main.o: $(SRCDIR)/main.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/test1.o: $(SRCDIR)/tests/test1.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/test2.o: $(SRCDIR)/tests/test2.c build
	$(CC) -c $(CFLAGS) $< -o $@
	
$(BUILDDIR)/test3.o: $(SRCDIR)/tests/test3.c build
	$(CC) -c $(CFLAGS) $< -o $@

$(BUILDDIR)/test4.o: $(SRCDIR)/tests/test4.c build
	$(CC) -c $(CFLAGS) $< -o $@

run: all $(BUILDDIR)/main
	./$(BUILDDIR)/main

clean:
	rm -rf $(BUILDDIR)


