#include "tests.h"

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "../mem.h"

/*
Этот тест покрывает случай очень простого использования:
    -выделяется небольшой блок
    -он используется
    -он освобождается
*/
void test1() {
  fprintf(stderr, "This is test1\n");
  void * allocated = _malloc(sizeof(int32_t) * 3);
  int32_t * arr = allocated;
  fprintf(stderr, "allocated pointer: %p\n", allocated);
  debug_heap(stderr, HEAP_START);
  arr[0] = 42;
  arr[2] = -228;
  fprintf(stderr, "numbers in array: %d %d %d\n", arr[0], arr[1], arr[2]);
  _free(arr);
  fprintf(stderr, "memory freed\n");
  debug_heap(stderr, HEAP_START);
}