#include "tests.h"

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>


#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/mman.h>

#include "../mem.h"

/*
Этот тест покрывает случай выделения нового региона не вплотную к старому
*/
#define VERY_BIG_QUERRY 16367
#define MAP_ANONYMOUS 32
static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

void test4() {
  fprintf(stderr, "----------------------------\nThis is test4\n");
  fprintf(stderr, "We are going to allocate very large block\n");
  void* const b1 = _malloc(VERY_BIG_QUERRY);
  debug_heap(stderr, HEAP_START);
  fprintf(stderr, "Now let's use map_pages to occupy memory after our region and alloc new block\n");
  void* const garbage = (char*) b1 + VERY_BIG_QUERRY;
  map_pages(garbage, 4096, 0);
  void* const b2 = _malloc(8175);
  debug_heap(stderr, HEAP_START);
  fprintf(stderr, "And let's free everything\n");
  _free(b2);
  _free(b1);
  debug_heap(stderr, HEAP_START);
}