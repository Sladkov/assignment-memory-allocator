#include "tests.h"

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "../mem.h"

/*
Этот тест покрывает случай выделения большого числа блоков

Выделяется много блоков

Выполняется тест 1

Освобождается часть блоков

Выполняется тест 1

Освобождается всё
*/
void test2() {
  fprintf(stderr, "----------------------------\nThis is test2\n");
  int32_t** array_2d = _malloc(sizeof(int32_t *) * 5);
  debug_heap(stderr, HEAP_START);
  for (size_t i = 0; i < 5; i++) {
      array_2d[i] = _malloc(sizeof(int32_t) * 100);
      for (size_t j = 0; j < 100; j++) {
          array_2d[i][j] = i*100 + j;
      }
  }
  fprintf(stderr, "2d array allocated and initialised\n");
  debug_heap(stderr, HEAP_START);
  test1();
  fprintf(stderr, "Let's free second line of 2d array\n");
  _free(array_2d[1]);
  debug_heap(stderr, HEAP_START);
  fprintf(stderr, "Let's free first line of 2d array\n");
  _free(array_2d[0]);
  debug_heap(stderr, HEAP_START);
  test1();
  fprintf(stderr, "Now let's free everything\n");
  for (size_t i = 2; i < 5; i++) {
      _free(array_2d[i]);
  }
  fprintf(stderr, "lines cleared\n");
  debug_heap(stderr, HEAP_START);
  _free(array_2d);
  fprintf(stderr, "memory freed\n");
  debug_heap(stderr, HEAP_START);
}