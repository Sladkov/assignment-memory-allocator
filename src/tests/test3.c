#include "tests.h"

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "../mem.h"

/*
Этот тест покрывает случай, когда требуется выделить новый регион и он выделяется
вплотную к старому
*/
void test3() {
  fprintf(stderr, "----------------------------\nThis is test3\n");
  fprintf(stderr, "We are going to allocate very large block\n");
  void* b1 = _malloc(8000);
  debug_heap(stderr, HEAP_START);
  fprintf(stderr, "And another one\n");
  void* b2 = _malloc(800);
  debug_heap(stderr, HEAP_START);
  _free(b1);
  _free(b2);
  fprintf(stderr, "Memory freed\n");
  debug_heap(stderr, HEAP_START);
  void* b3 = _malloc(10000);
  debug_heap(stderr, HEAP_START);
  _free(b3);
  debug_heap(stderr, HEAP_START);
}